import axios from './axios'

const SERVICE_URL = '/upload'

export const getMsg = () => {
  return axios.request({
    url: SERVICE_URL + '/upload/getMsg',
    method: 'get',
  })
}

// 上传文件，不能用qs 转换数据
import uploadAxios from 'axios'
export const uploadFile = (param) => {
  var configs = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
    onUploadProgress: (e) => {
      let completeProgress = (((e.loaded / e.total) * 100) | 0) + '%'
      console.log(completeProgress)
    },
  }
  return uploadAxios.post(SERVICE_URL + '/upload/uploadFile', param, configs)
}
