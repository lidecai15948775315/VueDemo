import Vue from 'vue'
import App from './App.vue'
import Routers from './router.js'
import VueRouter from 'vue-router'
import Meta from 'vue-meta'
import ViewUI from 'view-design'
import 'view-design/dist/styles/iview.css'
import axios from './api/axios'
import VueAxios from 'vue-axios'
// VueCropper
import VueCropper from 'vue-cropper'
// import 'cropperjs/dist/cropper.css';

Vue.use(VueAxios, axios)
  .use(VueRouter)
  .use(ViewUI)
  .use(Meta)
  // // VueCropper
  .use(VueCropper)
Vue.config.productionTip = false
new Vue({
  el: '#app',
  router: new VueRouter(Routers),
  render: (h) => h(App),
})
