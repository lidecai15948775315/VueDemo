// import Main from './RouterView.vue'
export default {
  routes: [
    {
      path: '/video',
      notShow: true,
      name: 'video',
      meta: {
        title: 'video',
      },
      component: () => import('./views/video/Video.vue'),
    },
    {
      path: '/uploadFolder',
      notShow: true,
      name: 'uploadFolder',
      meta: {
        title: 'uploadFolder',
      },
      component: () => import('./views/uploadFolder/uploadFolder.vue'),
    },
    {
      path: '/uploadFile',
      notShow: true,
      name: 'uploadFile',
      meta: {
        title: 'uploadFile',
      },
      component: () => import('./views/uploadFolder/uploadFile.vue'),
    },
    {
      path: '/vueTree',
      notShow: true,
      name: 'vueTree',
      meta: {
        title: 'vueTree',
      },
      component: () => import('./views/vueTree/VueTree.vue'),
    },
    {
      path: '/ofdview',
      notShow: true,
      name: 'ofdview',
      meta: {
        title: 'ofdview',
      },
      component: () => import('./views/ofdview/ofdview.vue'),
    },

    {
      path: '/mapTiles',
      notShow: true,
      name: 'mapTiles',
      meta: {
        title: 'mapTiles',
      },
      component: () => import('./views/map-tiles/Tile.vue'),
      // component: () => import('./views/map-tiles/Image.vue'),
    },
    {
      path: '/metaDataType',
      notShow: true,
      name: 'metaDataType',
      meta: {
        title: 'metaDataType',
      },
      component: () => import('./views/metaDataType/metaDataType.vue'),
      // component: () => import('./views/map-tiles/Image.vue'),
    },
  ],
}
